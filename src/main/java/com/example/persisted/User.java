package com.example.persisted;

import javax.persistence.*;

/**
 * Created by Alex on 03.09.2016.
 */

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    private String firstName;

    private String lastName;

}
